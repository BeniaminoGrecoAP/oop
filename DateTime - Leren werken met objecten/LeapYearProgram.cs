﻿using System;

namespace OOP
{
    internal class LeapYearProgram
    {
        public static void Main()
        {
            int leapAges = 0;

            for (int i = 1800; i <= 2020; i++)
            {
                DateTime date = new DateTime(i, 1, 1);
                if (DateTime.IsLeapYear(date.Year))
                {
                    leapAges++;
                }
            }

            Console.WriteLine($"Er zijn {leapAges} schrikkeljaren tussen 1800 en 2020.");
        }
    }
}