﻿using System;

namespace OOP
{
    internal class LerenWerkenMetDateTime
    {
        public static void StartSubmenu()
        {
            Console.Clear();
            Console.WriteLine("********************************");
            Console.WriteLine("Kies welke oefening je wil uitvoeren?");
            Console.WriteLine("********************************");
            Console.WriteLine("1. Welke dag van de week? (H8-dag-van-de-week)");
            Console.WriteLine("2. Aantal ticks sinds 2000? (H8-ticks-sinds-2000)");
            Console.WriteLine("3. Aantal schrikkeljaren tussen 1800 en 2020? (H8-schrikkelteller)");
            Console.WriteLine("4. Timen code? (H8-simpele-timing)");
            Console.Write("\rKeuze: ");

            int choice = int.Parse(Console.ReadLine());

            switch (choice)
            {
                case 1:
                    Console.Clear();
                    DayOfWeekProgram.Main();
                    break;

                case 2:
                    Console.Clear();
                    Ticks2000Program.Main();
                    break;

                case 3:
                    Console.Clear();
                    LeapYearProgram.Main();
                    break;

                case 4:
                    Console.Clear();
                    ArrayTimerProgram.Main();
                    break;

                default:
                    Console.Clear();
                    Console.WriteLine("Ongeldige keuze.");
                    break;
            }
        }
    }
}