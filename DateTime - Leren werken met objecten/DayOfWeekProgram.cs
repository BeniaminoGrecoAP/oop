﻿using System;
using System.Globalization;

namespace OOP
{
    internal class DayOfWeekProgram
    {
        public static void Main()
        {
            CultureInfo belgiumCI = new CultureInfo("nl-BE");

            Console.WriteLine("Welke dag wil je invoeren?");
            Console.Write("> ");

            int day = int.Parse(Console.ReadLine());

            Console.WriteLine("Welke maand wil je invoeren?");
            Console.Write("> ");
            int month = int.Parse(Console.ReadLine());

            Console.WriteLine("Welk jaar wil je invoeren?");
            Console.Write("> ");
            int year = int.Parse(Console.ReadLine());

            DateTime date = new DateTime(year, month, day);

            Console.WriteLine($"{date.ToString("dd MMMM yyyy", belgiumCI)} is een {date.ToString("dddd", belgiumCI)}.");
        }
    }
}