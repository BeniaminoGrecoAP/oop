﻿using System;

namespace OOP
{
    internal class ArrayTimerProgram
    {
        public static void Main()
        {
            DateTime before = DateTime.Now;

            int[] Array = new int[1000000];

            for (int i = 0; i < Array.Length; i++)
            {
                Array[i] = i + 1;
            }

            DateTime after = DateTime.Now;

            TimeSpan difference = after - before;

            Console.WriteLine($"Het duurt {difference.Milliseconds} milliseconden om een array van een miljoen elementen aan te maken en op te vullen met opeenvolgende waarden.");
        }
    }
}