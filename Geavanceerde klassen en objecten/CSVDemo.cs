﻿using System;
using System.IO;
using System.Net;

namespace OOP
{
    internal static class CSVDemo
    {
        public static void Run()
        {
            WebClient wc = new WebClient();
            string csv = wc.DownloadString("http://samplecsvs.s3.amazonaws.com/SalesJan2009.csv");

            string[] split = csv.Split('\r');

            for (int i = 1; i < split.Length; i++)
            {
                string[] lijnsplit = split[i].Split(',');
                Console.Write("Transaction date = " + lijnsplit[0] + "\t\t");
                Console.Write("Product = " + lijnsplit[1] + "\t\t");
                Console.WriteLine("Price = " + lijnsplit[2]);
            }

            Console.ReadKey();
        }

        public static void WriteCSV()
        {
            string[] namen = { "Tim", "Jos", "Mo" };
            int[] leeftijden = { 34, 76, 23 };

            string[] lines = new string[namen.Length];
            for (int i = 0; i < lines.Length; i++)
            {
                lines[i] = $"{i},{namen[i]},{leeftijden[i]}";
            }

            File.WriteAllLines(@"D:\test\ages.csv", lines);
        }
    }
}