﻿using System;

namespace OOP
{
    internal class GeavanceerdeKlassenEnObjecten
    {
        public static void StartSubmenu()
        {
            Console.Clear();
            Console.WriteLine("********************************");
            Console.WriteLine("Welke oefening wil je uitvoeren?");
            Console.WriteLine("********************************");
            Console.WriteLine("1. Pokemon makkelijk aanmaken (H10-PokeConstructie)");
            Console.WriteLine("2. Pokemon nog makkelijker aanmaken (H10-Chaining)");
            Console.WriteLine("3. Globale statistieken bijhouden (H10-pokebattelecount)");
            Console.WriteLine("4. Gemeenschappelijke kenmerken (H10-tombola)");
            Console.WriteLine("5. CSV (H10-Downloaden CSV bestand)");
            Console.WriteLine("6. CSV (H10-Schrijven CSV bestand)");
            int choice = int.Parse(Console.ReadLine());

            switch (choice)
            {
                case 1:
                    Console.Clear();
                    PokemonH10.MakePokemon();
                    break;

                case 2:
                    Console.Clear();
                    PokemonH10.ConstructPokemonChained();
                    break;

                case 3:
                    Console.Clear();
                    PokemonH10.DemonstrateCounter();
                    break;

                case 4:
                    Console.Clear();
                    Ticket.Raffle();
                    break;

                case 5:
                    Console.Clear();
                    CSVDemo.Run();
                    break;

                case 6:
                    Console.Clear();
                    CSVDemo.WriteCSV();
                    break;

                default:
                    Console.WriteLine("Ongeldige keuze.");
                    break;
            }
        }
    }
}