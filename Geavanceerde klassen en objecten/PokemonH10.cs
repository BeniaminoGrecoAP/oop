﻿using System;

namespace OOP
{
    internal class PokemonH10
    {
        private static int countPokeTypeGrass = 0;
        private static int countPokeTypeFire = 0;
        private static int countPokeTypeWater = 0;
        private static int countPokeTypeElectric = 0;

        private int maxHP;

        public int MaxHP
        {
            get { return maxHP; }
            set
            {
                if (value < 20)
                {
                    maxHP = 20;
                }
                else if (value > 1000)
                {
                    maxHP = 1000;
                }
                else
                {
                    maxHP = value;
                }
            }
        }

        private int hP;

        public int HP
        {
            get { return hP; }
            set
            {
                if (value < 0)
                {
                    hP = 0;
                }
                else if (value > MaxHP)
                {
                    hP = MaxHP;
                }
                else
                {
                    hP = value;
                }
            }
        }

        public PokeSpecies PokeSpecies { get; set; }
        public PokeTypes PokeType { get; set; }

        public PokemonH10()
        {
        }

        public PokemonH10(int maxHp, int hp, PokeSpecies pokeSpecies, PokeTypes pokeType)
        {
            this.MaxHP = maxHp;
            this.HP = hp;
            this.PokeSpecies = pokeSpecies;
            this.PokeType = pokeType;
        }

        /// <summary>
        /// H10-chaining
        /// </summary>
        /// <param name="maxHp"></param>
        /// <param name="pokeSpecies"></param>
        /// <param name="pokeType"></param>
        public PokemonH10(int maxHp, PokeSpecies pokeSpecies, PokeTypes pokeType)
            : this(maxHp, 0, pokeSpecies, pokeType)
        {
            this.HP = maxHp / 2;
        }

        public void Attack()
        {
            switch (PokeType)
            {
                case PokeTypes.Grass:
                    AttackText(ConsoleColor.Green);
                    countPokeTypeGrass++;
                    break;

                case PokeTypes.Fire:
                    AttackText(ConsoleColor.Red);
                    countPokeTypeFire++;
                    break;

                case PokeTypes.Water:
                    AttackText(ConsoleColor.Blue);
                    countPokeTypeWater++;
                    break;

                case PokeTypes.Electric:
                    AttackText(ConsoleColor.Yellow);
                    countPokeTypeElectric++;
                    break;

                default:
                    break;
            }
        }

        /// <summary>
        /// drukt aanvaller op scherm af in doorgegeven kleur
        /// </summary>
        /// <param name="color">kleur van aanvaller</param>
        private void AttackText(ConsoleColor color)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(PokeSpecies.ToString().ToUpper());
            Console.ResetColor();
        }

        /// <summary>
        /// H10-constructor
        /// </summary>
        public static void MakePokemon()
        {
            PokemonH10 bulbasaur = new PokemonH10(20, 20, PokeSpecies.Bulbasaur, PokeTypes.Grass);
            PokemonH10 charmander = new PokemonH10(20, 20, PokeSpecies.Charmander, PokeTypes.Fire);
            PokemonH10 squirtle = new PokemonH10(20, 20, PokeSpecies.Squirtle, PokeTypes.Water);
            PokemonH10 pikachu = new PokemonH10(20, 20, PokeSpecies.Pikachu, PokeTypes.Electric);

            bulbasaur.Attack();
            charmander.Attack();
            squirtle.Attack();
            pikachu.Attack();
        }

        /// <summary>
        /// H10-Chaining
        /// </summary>
        public static void ConstructPokemonChained()
        {
            PokemonH10 pokemonH9 = new PokemonH10(40, PokeSpecies.Charmander, PokeTypes.Electric);

            Console.WriteLine($"De nieuwe {pokemonH9.PokeSpecies} heeft een maximum van {pokemonH9.MaxHP}HP en heeft momenteel {pokemonH9.HP}HP.");
        }

        public static void DemonstrateCounter()
        {
            PokemonH10 bulbasaur = new PokemonH10();
            bulbasaur.MaxHP = 20;
            bulbasaur.hP = 20;
            bulbasaur.PokeSpecies = PokeSpecies.Bulbasaur;
            bulbasaur.PokeType = PokeTypes.Grass;
            PokemonH10 charmander = new PokemonH10();
            charmander.MaxHP = 20;
            charmander.hP = 20;
            charmander.PokeSpecies = PokeSpecies.Charmander;
            charmander.PokeType = PokeTypes.Fire;
            PokemonH10 squirtle = new PokemonH10();
            squirtle.MaxHP = 20;
            squirtle.hP = 20;
            squirtle.PokeSpecies = PokeSpecies.Squirtle;
            squirtle.PokeType = PokeTypes.Water;
            PokemonH10 pikachu = new PokemonH10();
            pikachu.MaxHP = 20;
            pikachu.hP = 20;
            pikachu.PokeSpecies = PokeSpecies.Pikachu;
            pikachu.PokeType = PokeTypes.Electric;
            PokemonH10 bulbasaur2 = new PokemonH10();
            bulbasaur2.MaxHP = 20;
            bulbasaur2.hP = 20;
            bulbasaur2.PokeSpecies = PokeSpecies.Bulbasaur;
            bulbasaur2.PokeType = PokeTypes.Grass;

            bulbasaur.Attack();
            bulbasaur.Attack();
            bulbasaur.Attack();
            bulbasaur.Attack();
            bulbasaur.Attack();
            squirtle.Attack();
            squirtle.Attack();
            squirtle.Attack();
            squirtle.Attack();
            squirtle.Attack();
            pikachu.Attack();
            pikachu.Attack();
            pikachu.Attack();
            pikachu.Attack();
            pikachu.Attack();
            pikachu.Attack();
            charmander.Attack();
            charmander.Attack();
            charmander.Attack();
            charmander.Attack();
            charmander.Attack();
            charmander.Attack();
            bulbasaur2.Attack();
            bulbasaur2.Attack();
            bulbasaur2.Attack();
            bulbasaur2.Attack();
            bulbasaur2.Attack();
            bulbasaur2.Attack();
            bulbasaur2.Attack();

            Console.WriteLine($"Aantal aanvallen van het Pokemon met type 'grass' : {countPokeTypeGrass}");
            Console.WriteLine($"Aantal aanvallen van het Pokemon met type 'fire' : {countPokeTypeFire}");
            Console.WriteLine($"Aantal aanvallen van het Pokemon met type 'water' : {countPokeTypeWater}");
            Console.WriteLine($"Aantal aanvallen van het Pokemon met type 'electric' : {countPokeTypeElectric}");
        }
    }
}