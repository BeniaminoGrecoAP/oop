﻿using System;

namespace OOP
{
    internal class Ticket
    {
        private static Random random = new Random();
        public byte Prize { get; set; }

        public Ticket()
        {
            this.Prize = (byte)random.Next(1, 101);
        }

        public static void Raffle()
        {
            Ticket ticket1 = new Ticket();
            Console.WriteLine($"waarde van het lotje is {ticket1.Prize}");
            Ticket ticket2 = new Ticket();
            Console.WriteLine($"waarde van het lotje is {ticket2.Prize}");
            Ticket ticket3 = new Ticket();
            Console.WriteLine($"waarde van het lotje is {ticket3.Prize}");
            Ticket ticket4 = new Ticket();
            Console.WriteLine($"waarde van het lotje is {ticket4.Prize}");
            Ticket ticket5 = new Ticket();
            Console.WriteLine($"waarde van het lotje is {ticket5.Prize}");
            Ticket ticket6 = new Ticket();
            Console.WriteLine($"waarde van het lotje is {ticket6.Prize}");
            Ticket ticket7 = new Ticket();
            Console.WriteLine($"waarde van het lotje is {ticket7.Prize}");
            Ticket ticket8 = new Ticket();
            Console.WriteLine($"waarde van het lotje is {ticket8.Prize}");
            Ticket ticket9 = new Ticket();
            Console.WriteLine($"waarde van het lotje is {ticket9.Prize}");
            Ticket ticket10 = new Ticket();
            Console.WriteLine($"waarde van het lotje is {ticket10.Prize}");
        }
    }
}