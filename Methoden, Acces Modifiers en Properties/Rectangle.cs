﻿using System;

namespace OOP
{
    internal class Rectangle
    {
        private double width = 1;

        public double Width
        {
            get
            {
                return width;
            }
            set
            {
                if (value <= 0)
                {
                    Console.WriteLine($"Het is verboden om een breedte van {value} in te stellen!");
                }
                else
                {
                    width = value;
                }
            }
        }

        private double height = 1;

        public double Height
        {
            get
            {
                return height;
            }
            set
            {
                if (value <= 0)
                {
                    Console.WriteLine($"Het is verboden om een hoogte van {value} in te stellen!");
                }
                else
                {
                    height = value;
                }
            }
        }

        private double surface;

        public double Surface
        {
            get { return Height * Width; }
        }
    }
}