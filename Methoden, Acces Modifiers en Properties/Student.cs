﻿using System;

namespace OOP
{
    internal class Student
    {
        public string Name { get; set; }
        private byte age;

        public byte Age
        {
            get { return age; }
            set
            {
                if (value <= 120)
                {
                    age = value;
                }
            }
        }

        public ClassGroups ClassGroup { get; set; }
        private byte markCommunication;

        public byte MarkCommunication
        {
            get { return markCommunication; }
            set
            {
                if (value <= 20)
                {
                    markCommunication = value;
                }
            }
        }

        private byte markProgrammingPrinciples;

        public byte MarkProgrammingPrinciples
        {
            get { return markProgrammingPrinciples; }
            set
            {
                if (value <= 20)
                {
                    markProgrammingPrinciples = value;
                }
            }
        }

        private byte markWebTech;

        public byte MarkWebTech
        {
            get { return markWebTech; }
            set
            {
                if (value <= 20)
                {
                    markWebTech = value;
                }
            }
        }

        private double overallMark;

        public double OverallMark
        {
            get { return (MarkCommunication + MarkProgrammingPrinciples + MarkWebTech) / 3; }
        }

        public void ShowOverview()
        {
            Console.WriteLine($"{this.Name}, {this.Age} jaar");
            Console.WriteLine($"Klas: {this.ClassGroup}");
            Console.WriteLine();
            Console.WriteLine($"Cijferrapport:");
            Console.WriteLine($"**************");
            Console.WriteLine($"Communicatie \t\t {this.MarkCommunication}");
            Console.WriteLine($"Programming Principles \t {this.MarkProgrammingPrinciples}");
            Console.WriteLine($"Web Technology \t\t {this.MarkWebTech}");
            Console.WriteLine($"Gemiddelde: \t\t {this.OverallMark}");
        }

        public static void Main()
        {
            Student student1 = new Student();
            student1.ClassGroup = ClassGroups.EA2;
            student1.Age = 21;
            student1.Name = "Joske Vermeulen";
            student1.MarkCommunication = 12;
            student1.MarkProgrammingPrinciples = 15;
            student1.MarkWebTech = 13;
            student1.ShowOverview();
        }
    }
}