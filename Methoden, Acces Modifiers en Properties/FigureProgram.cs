﻿using System;

namespace OOP
{
    internal class FigureProgram
    {
        public static void Main()
        {
            Rectangle rectangle0 = new Rectangle();
            rectangle0.Height = -1;
            rectangle0.Width = 0;
            //  Console.WriteLine($"Een rechthoek met een breedte van {rectangle0.Width} en een hoogte van {rectangle0.Height} heeft een oppervlakte van {rectangle0.Surface}");

            Rectangle rectangle1 = new Rectangle();
            rectangle1.Height = 2.2;
            rectangle1.Width = 1.5;
            Console.WriteLine($"Een rechthoek met een breedte van {rectangle1.Width} en een hoogte van {rectangle1.Height} heeft een oppervlakte van {rectangle1.Surface.ToString("F2")}");

            Rectangle rectangle2 = new Rectangle();
            rectangle2.Height = 3;
            rectangle2.Width = 1;
            Console.WriteLine($"Een rechthoek met een breedte van {rectangle2.Width} en een hoogte van {rectangle2.Height} heeft een oppervlakte van {rectangle2.Surface.ToString("F2")}");

            Triangle triangle0 = new Triangle();
            triangle0.Height = -2;
            triangle0.Base = -3;

            Triangle triangle1 = new Triangle();
            triangle1.Height = 2;
            triangle1.Base = 3;
            Console.WriteLine($"Een driehoek met een basis van {triangle1.Base} en een hoogte van {triangle1.Height} heeft een oppervlakte van {triangle1.Surface.ToString("F2")}");

            Triangle triangle2 = new Triangle();
            triangle2.Height = 3;
            triangle2.Base = 5;
            Console.WriteLine($"Een driehoek met een basis van {triangle2.Base} en een hoogte van {triangle2.Height} heeft een oppervlakte van {triangle2.Surface.ToString("F2")}");
        }
    }
}