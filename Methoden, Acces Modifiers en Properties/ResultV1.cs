﻿using System;

namespace OOP
{
    internal class ResultV1

    {
        private int result;

        public int Result
        {
            get { return result; }
            set { result = value; }
        }

        public static void Main()
        {
            ResultV1 first = new ResultV1();
            first.Result = 30;
            first.PrintHonors();

            ResultV1 second = new ResultV1();
            second.Result = 55;
            second.PrintHonors();

            ResultV1 third = new ResultV1();
            third.Result = 69;
            third.PrintHonors();

            ResultV1 fourth = new ResultV1();
            fourth.Result = 79;
            fourth.PrintHonors();

            ResultV1 fifth = new ResultV1();
            fifth.Result = 94;
            fifth.PrintHonors();
        }

        public void PrintHonors()
        {
            if (Result < 50)
            {
                Console.WriteLine("niet geslaagd");
            }
            else if (Result <= 58)
            {
                Console.WriteLine("voldoende");
            }
            else if (Result <= 75)
            {
                Console.WriteLine("onderscheiding");
            }
            else if (Result <= 85)
            {
                Console.WriteLine("grote onderscheiding");
            }
            else
            {
                Console.WriteLine("grootste onderscheiding");
            }
        }
    }
}