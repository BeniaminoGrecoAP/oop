﻿using System;

namespace OOP
{
    internal class ResultV3
    {
        private int result;

        public int Result
        {
            get { return result; }
            set
            {
                if (value > 0 && value < 100)
                {
                    result = value;
                }
            }
        }

        private Honors computeHonors;

        public Honors ComputeHonors
        {
            get
            {
                if (Result < 50)
                {
                    return Honors.NietGeslaagd;
                }
                else if (Result <= 68)
                {
                    return Honors.Voldoende;
                }
                else if (Result <= 75)
                {
                    return Honors.Onderscheiding;
                }
                else if (Result <= 85)
                {
                    return Honors.GroteOnderscheiding;
                }
                else
                {
                    return Honors.GrootsteOnderscheiding;
                }
            }
        }

        public static void Main()
        {
            ResultV3 first = new ResultV3();
            first.Result = 45;
            Console.WriteLine(first.ComputeHonors);

            ResultV3 second = new ResultV3();
            second.Result = 55;
            Console.WriteLine(second.ComputeHonors);

            ResultV3 third = new ResultV3();
            third.Result = 72;
            Console.WriteLine(third.ComputeHonors);

            ResultV3 fourth = new ResultV3();
            fourth.Result = 83;
            Console.WriteLine(fourth.ComputeHonors);

            ResultV3 fifth = new ResultV3();
            fifth.Result = 93;
            Console.WriteLine(fifth.ComputeHonors);
        }
    }
}