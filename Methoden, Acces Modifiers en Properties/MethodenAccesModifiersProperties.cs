﻿using System;

namespace OOP
{
    internal class MethodenAccesModifiersProperties
    {
        public static void StartSubmenu()
        {
            Console.Clear();
            Console.WriteLine("********************************");
            Console.WriteLine("Welke oefening wil je uitvoeren?");
            Console.WriteLine("********************************");
            Console.WriteLine("1. Printen resultaten V1 (H8-RapportModule-V1)");
            Console.WriteLine("2. Printen resultaten V2 (H8-RapportModule-V2)");
            Console.WriteLine("3. Getallencombinaties maken (H8-Getallencombinatie)");
            Console.WriteLine("4. Berekenen van oppervlaktes (H8-Figuren)");
            Console.WriteLine("5. Studenten toevoegen(H8-Studentklasse)");
            Console.WriteLine("6. Printen resultaten V3 (H8-RapportModule-V3)");
            int choice = int.Parse(Console.ReadLine());

            switch (choice)
            {
                case 1:
                    Console.Clear();
                    ResultV1.Main();
                    break;

                case 2:
                    Console.Clear();
                    ResultV2.Main();
                    break;

                case 3:
                    Console.Clear();
                    NumberCombination.Main();
                    break;

                case 4:
                    Console.Clear();
                    FigureProgram.Main();
                    break;

                case 5:
                    Console.Clear();
                    Student.Main();
                    break;

                case 6:
                    Console.Clear();
                    ResultV3.Main();
                    break;

                default:
                    Console.WriteLine("Ongeldige keuze.");
                    break;
            }
        }
    }
}