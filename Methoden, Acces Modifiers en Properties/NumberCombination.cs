﻿using System;

namespace OOP
{
    internal class NumberCombination
    {
        private int nubmer1;

        public int Number1
        {
            get { return nubmer1; }
            set { nubmer1 = value; }
        }

        private int number2;

        public int Number2
        {
            get { return number2; }
            set { number2 = value; }
        }

        public static void Main()
        {
            NumberCombination pair1 = new NumberCombination();
            pair1.Number1 = 12;
            pair1.Number2 = 34;
            Console.WriteLine("Paar:" + pair1.Number1 + ", " + pair1.Number2);
            Console.WriteLine("Sum = " + pair1.Sum());
            Console.WriteLine("Verschil = " + pair1.Difference());
            Console.WriteLine("Product = " + pair1.Product());
            Console.WriteLine("Quotient = " + pair1.Quotient());
        }

        public double Sum()
        {
            return (double)Number1 + (double)number2;
        }

        public double Difference()
        {
            return (double)Number1 - (double)number2;
        }

        public double Product()
        {
            return (double)Number1 * (double)number2;
        }

        public double Quotient()
        {
            if (number2 == 0)
            {
                Console.WriteLine("Error!");
                return 0.0;
            }

            return Math.Round((double)Number1 / (double)number2, 7);
        }
    }
}