﻿namespace OOP
{
    internal enum Honors
    {
        NietGeslaagd,
        Voldoende,
        Onderscheiding,
        GroteOnderscheiding,
        GrootsteOnderscheiding
    }
}