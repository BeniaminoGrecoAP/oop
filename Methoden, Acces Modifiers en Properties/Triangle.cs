﻿using System;

namespace OOP
{
    internal class Triangle
    {
        private double basis = 1;

        public double Base
        {
            get { return basis; }
            set
            {
                if (value <= 0)
                {
                    Console.WriteLine($"Het is verboden een breedte in te stellen van {value}");
                }
                else
                {
                    basis = value;
                }
            }
        }

        private double height = 1;

        public double Height
        {
            get { return height; }
            set
            {
                if (value <= 0)
                {
                    Console.WriteLine($"Het is verboden een hoogte in te stellen van {value}");
                }
                else
                {
                    height = value;
                }
            }
        }

        private double surface;

        public double Surface
        {
            get { return Height * Base / 2; }
        }
    }
}