﻿using System;

namespace OOP
{
    internal class ResultV2
    {
        public int Result { get; set; }

        public static void Main()
        {
            ResultV2 first = new ResultV2();
            first.Result = 45;
            Console.WriteLine(first.ComputeHonors());

            ResultV2 second = new ResultV2();
            second.Result = 55;
            Console.WriteLine(second.ComputeHonors());

            ResultV2 third = new ResultV2();
            third.Result = 72;
            Console.WriteLine(third.ComputeHonors());

            ResultV2 fourth = new ResultV2();
            fourth.Result = 83;
            Console.WriteLine(fourth.ComputeHonors());

            ResultV2 fifth = new ResultV2();
            fifth.Result = 93;
            Console.WriteLine(fifth.ComputeHonors());
        }

        public Honors ComputeHonors()
        {
            if (Result < 50)
            {
                return Honors.NietGeslaagd;
            }
            else if (Result <= 68)
            {
                return Honors.Voldoende;
            }
            else if (Result <= 75)
            {
                return Honors.Onderscheiding;
            }
            else if (Result <= 85)
            {
                return Honors.GroteOnderscheiding;
            }
            else
            {
                return Honors.GrootsteOnderscheiding;
            }
        }
    }
}