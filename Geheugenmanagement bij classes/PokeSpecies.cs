﻿namespace OOP
{
    internal enum PokeSpecies
    {
        Bulbasaur, Charmander, Squirtle, Pikachu
    }
}