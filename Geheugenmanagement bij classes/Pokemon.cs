﻿using System;

namespace OOP
{
    internal class Pokemon
    {
        private int maxHP;

        public int MaxHP
        {
            get { return maxHP; }
            set
            {
                if (value < 20)
                {
                    maxHP = 20;
                }
                else if (value > 1000)
                {
                    maxHP = 1000;
                }
                else
                {
                    maxHP = value;
                }
            }
        }

        private int hP;

        public int HP
        {
            get { return hP; }
            set
            {
                if (value < 0)
                {
                    hP = 0;
                }
                else if (value > MaxHP)
                {
                    hP = MaxHP;
                }
                else
                {
                    hP = value;
                }
            }
        }

        public PokeSpecies PokeSpecies { get; set; }
        public PokeTypes PokeType { get; set; }

        public void Attack()
        {
            switch (PokeType)
            {
                case PokeTypes.Grass:
                    AttackText(ConsoleColor.Green);
                    break;

                case PokeTypes.Fire:
                    AttackText(ConsoleColor.Red);
                    break;

                case PokeTypes.Water:
                    AttackText(ConsoleColor.Blue);
                    break;

                case PokeTypes.Electric:
                    AttackText(ConsoleColor.Yellow);
                    break;

                default:
                    break;
            }
        }

        /// <summary>
        /// drukt aanvaller op scherm af in doorgegeven kleur
        /// </summary>
        /// <param name="color">kleur van aanvaller</param>
        private void AttackText(ConsoleColor color)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(PokeSpecies.ToString().ToUpper());
            Console.ResetColor();
        }

        public static void MakePokemon()
        {
            Pokemon bulbasaur = new Pokemon();
            bulbasaur.MaxHP = 20;
            bulbasaur.hP = 20;
            bulbasaur.PokeSpecies = PokeSpecies.Bulbasaur;
            bulbasaur.PokeType = PokeTypes.Grass;
            Pokemon charmander = new Pokemon();
            charmander.MaxHP = 20;
            charmander.hP = 20;
            charmander.PokeSpecies = PokeSpecies.Charmander;
            charmander.PokeType = PokeTypes.Fire;
            Pokemon squirtle = new Pokemon();
            squirtle.MaxHP = 20;
            squirtle.hP = 20;
            squirtle.PokeSpecies = PokeSpecies.Squirtle;
            squirtle.PokeType = PokeTypes.Water;
            Pokemon pikachu = new Pokemon();
            pikachu.MaxHP = 20;
            pikachu.hP = 20;
            pikachu.PokeSpecies = PokeSpecies.Pikachu;
            pikachu.PokeType = PokeTypes.Electric;
            bulbasaur.Attack();
            charmander.Attack();
            squirtle.Attack();
            pikachu.Attack();
        }

        /// <summary>
        /// door array gaan en eerste pokemen met hp>0 teruggeven
        /// ander null teruggeven
        /// </summary>
        /// <param name="pokemon"></param>
        /// <returns></returns>
        public static Pokemon FirstConsciousPokemon(Pokemon[] pokemon) //Aanmaak van methode: we geven hier een array v Pokemon met pokemons mee
        {
            for (int i = 0; i < pokemon.Length; i++)
            {
                if (pokemon[i].HP > 0) // als we er ene vinden, wat is zijne hp? Je kan door hier ne breakpoint te zetten, zien in welke volgorde hij het doorloopt
                {
                    return pokemon[i];
                }
            }
            return null;
        }

        public static void TestConsciousPokemon()
        {
            Pokemon bulbasaur = new Pokemon();
            bulbasaur.MaxHP = 20;
            bulbasaur.hP = 0;
            bulbasaur.PokeSpecies = PokeSpecies.Bulbasaur;
            bulbasaur.PokeType = PokeTypes.Grass;
            Pokemon charmander = new Pokemon();
            charmander.MaxHP = 0;
            charmander.hP = 20;
            charmander.PokeSpecies = PokeSpecies.Charmander;
            charmander.PokeType = PokeTypes.Fire;
            Pokemon squirtle = new Pokemon();
            squirtle.MaxHP = 20;
            squirtle.hP = 20;
            squirtle.PokeSpecies = PokeSpecies.Squirtle;
            squirtle.PokeType = PokeTypes.Water;
            Pokemon pikachu = new Pokemon();
            pikachu.MaxHP = 20;
            pikachu.hP = 20;
            pikachu.PokeSpecies = PokeSpecies.Pikachu;
            pikachu.PokeType = PokeTypes.Electric;

            //Pokemon[] pokemon = new Pokemon[4];
            //pokemon[0] = squirtle;
            //pokemon[1] = charmander;
            //pokemon[2] = squirtle;
            //pokemon[3] = pikachu;

            Pokemon[] pokemon = { squirtle, charmander, squirtle, pikachu };
			
			  //   pokemons[0].HP  zo kan je van bulbasaur of andere poke al zijn waardes oproepen.

            FirstConsciousPokemon(pokemon).Attack(); // zie comment hierboven
        }

        /// <summary>
        /// methode maakt array van pokemon en roept methode aan om deze array te gaan
        /// en geeft de eerste bewuste pokemon door
        /// als er geen bewuste pokemon is geeft hij dit als boodschap weer
        /// </summary>
        public static void TestConsciousPokemonSafe()
        {
            Pokemon bulbasaur = new Pokemon();
            bulbasaur.MaxHP = 20;
            bulbasaur.HP = 0;
            bulbasaur.PokeSpecies = PokeSpecies.Bulbasaur;
            bulbasaur.PokeType = PokeTypes.Grass;
            Pokemon charmander = new Pokemon();
            charmander.MaxHP = 0;
            charmander.hP = 0;
            charmander.PokeSpecies = PokeSpecies.Charmander;
            charmander.PokeType = PokeTypes.Fire;
            Pokemon squirtle = new Pokemon();
            squirtle.MaxHP = 20;
            squirtle.hP = 15;
            squirtle.PokeSpecies = PokeSpecies.Squirtle;
            squirtle.PokeType = PokeTypes.Water;
            Pokemon pikachu = new Pokemon();
            pikachu.MaxHP = 20;
            pikachu.hP = 0;
            pikachu.PokeSpecies = PokeSpecies.Pikachu;
            pikachu.PokeType = PokeTypes.Electric;

            //Pokemon[] pokemon = new Pokemon[4];
            //pokemon[0] = squirtle;
            //pokemon[1] = charmander;
            //pokemon[2] = squirtle;
            //pokemon[3] = pikachu;

            Pokemon[] pokemon = { squirtle, charmander, squirtle, pikachu };

            if (FirstConsciousPokemon(pokemon) == null)
            {
                Console.WriteLine("Al je Pokemon zijn KO! Haast je naar het Pokemon Center.");
            }
            else
            {
                FirstConsciousPokemon(pokemon).Attack();
            }
        }

        /// <summary>
        /// verhoogt de HP van pokemon tot opgegeven waarde
        /// </summary>
        /// <param name="pokemon">pokemon waarvan HP verhoogt moet worden</param>
        /// <param name="newHP">waarde tot waar moet verhoogt worden</param>
        public static void RestoreHP(Pokemon pokemon, int newHP)
        {
            pokemon.HP = newHP;    //oldHP = newHP is op de stack; datatype binne methode is kenbaar, maar buiten methode niet. is by value en niet met de referentie
        }

        public static void DemoRestoreHP()
        {
            Pokemon bulbasaur = new Pokemon();
            bulbasaur.MaxHP = 20;
            bulbasaur.hP = 0;
            bulbasaur.PokeSpecies = PokeSpecies.Bulbasaur;
            bulbasaur.PokeType = PokeTypes.Grass;
            Pokemon charmander = new Pokemon();
            charmander.MaxHP = 0;
            charmander.hP = 0;
            charmander.PokeSpecies = PokeSpecies.Charmander;
            charmander.PokeType = PokeTypes.Fire;
            Pokemon squirtle = new Pokemon();
            squirtle.MaxHP = 20;
            squirtle.hP = 0;
            squirtle.PokeSpecies = PokeSpecies.Squirtle;
            squirtle.PokeType = PokeTypes.Water;
            Pokemon pikachu = new Pokemon();
            pikachu.MaxHP = 70;
            pikachu.hP = 0;
            pikachu.PokeSpecies = PokeSpecies.Pikachu;
            pikachu.PokeType = PokeTypes.Electric;

            Pokemon[] pokemon = { squirtle, charmander, squirtle, pikachu };

            foreach (Pokemon poke in pokemon)
            {
                /*Pokemon.*/
                RestoreHP(poke, 50);
            }
            //afdrukken van alle hp van de pokemon
            for (int i = 0; i < pokemon.Length; i++)
            {
                Console.WriteLine(pokemon[i].HP);
            }
        }

        /// <summary>
        /// 2 pokemon vechten tegen elkaar, de uitslag van de eerste
        /// pokemon wordt op het scherm getoond
        /// </summary>
        /// <param name="poke1"></param>
        /// <param name="poke2"></param>
        /// <param name="random"></param>
        public static void FightOutcome(Pokemon poke1, Pokemon poke2, Random random)
        {
            int starter = random.Next(0, 2);
            bool play = true;

            if ((poke1.HP == 0 || poke1 == null) && (poke2.HP == 0 || poke2 == null))
            {
                Console.WriteLine($"{poke1.PokeSpecies}, {FightDecision.UNDECIDED}");
            }
            else if ((poke1.HP != 0 || poke1 != null) && (poke2.HP == 0 || poke2 == null))
            {
                Console.WriteLine($"{poke1.PokeSpecies}, {FightDecision.WIN}");
            }
            else if ((poke1.HP == 0 || poke1 == null) && (poke2.HP != 0 || poke2 != null))
            {
                Console.WriteLine($"{poke1.PokeSpecies}, {FightDecision.LOSS}");
            }
            else
            {
                while (play)
                {
                    //aanvaller wordt elke beurt gewijzigd
                    starter = (starter + 1) % 2;

                    if (starter == 0)
                    {
                        poke1.Attack();
                        int aanvalsPunten = random.Next(0, 21);
                        poke2.HP -= aanvalsPunten;
                        Console.WriteLine($"{poke2.PokeSpecies} aangevallen met {aanvalsPunten}, nu nog {poke2.HP}HP over.");
                    }
                    else if (starter == 1)
                    {
                        poke2.Attack();
                        int aanvalsPunten = random.Next(0, 21);
                        poke1.HP -= aanvalsPunten;
                        Console.WriteLine($"{poke1.PokeSpecies} aangevallen met {aanvalsPunten}, nu nog {poke1.HP}HP over.");
                    }
                    if (poke1.HP == 0)
                    {
                        play = false;
                        Console.WriteLine($"{poke1.PokeSpecies}, {FightDecision.LOSS}");
                    }
                    else if (poke2.HP == 0)
                    {
                        play = false;
                        Console.WriteLine($"{poke1.PokeSpecies}, {FightDecision.WIN}");
                    }
                }
            }
        }

        public static void DemoFightOutcome()
        {
            Pokemon bulbasaur = new Pokemon();
            bulbasaur.MaxHP = 20;
            bulbasaur.HP = 20;
            bulbasaur.PokeSpecies = PokeSpecies.Bulbasaur;
            bulbasaur.PokeType = PokeTypes.Grass;
            Pokemon charmander = new Pokemon();
            charmander.MaxHP = 0;
            charmander.HP = 20;
            charmander.PokeSpecies = PokeSpecies.Charmander;
            charmander.PokeType = PokeTypes.Fire;
            Pokemon squirtle = new Pokemon();
            squirtle.MaxHP = 20;
            squirtle.HP = 0;
            squirtle.PokeSpecies = PokeSpecies.Squirtle;
            squirtle.PokeType = PokeTypes.Water;
            Pokemon pikachu = new Pokemon();
            pikachu.MaxHP = 70;
            pikachu.HP = 20;
            pikachu.PokeSpecies = PokeSpecies.Pikachu;
            pikachu.PokeType = PokeTypes.Electric;

            Pokemon[] pokemon = { bulbasaur, charmander, squirtle, pikachu };

            Random random = new Random();

            FightOutcome(pokemon[0], pokemon[3], random);
        }
    }
}