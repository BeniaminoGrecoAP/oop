﻿using System;

namespace OOP
{
    internal class GeheugenmanagementBijClasses
    {
        public static void StartSubmenu()
        {
            Console.Clear();
            Console.WriteLine("********************************");
            Console.WriteLine("Welke oefening wil je uitvoeren?");
            Console.WriteLine("********************************");
            Console.WriteLine("1. Pokemon (H9-pokeattack)");
            Console.WriteLine("2. Pokemon (H9-consciouspokemon-Improved)");
            Console.WriteLine("3. Pokemon (H9-pokevalueref)");
            Console.WriteLine("4. Pokemon (H9-fight)");
            int choice = int.Parse(Console.ReadLine());

            switch (choice)
            {
                case 1:
                    Console.Clear();
                    Pokemon.MakePokemon();
                    break;

                case 2:
                    Console.Clear();
                    Pokemon.TestConsciousPokemonSafe();
                    break;

                case 3:
                    Console.Clear();
                    Pokemon.DemoRestoreHP();
                    break;

                case 4:
                    Console.Clear();
                    Pokemon.DemoFightOutcome();
                    break;

                default:
                    Console.WriteLine("Ongeldige keuze.");
                    break;
            }
        }
    }
}