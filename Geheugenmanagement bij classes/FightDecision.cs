﻿namespace OOP
{
    internal enum FightDecision
    {
        WIN, LOSS, UNDECIDED
    }
}