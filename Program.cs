﻿using System;

namespace OOP
{
    public class Program
    {
        public static void Main(string[] args)
        {
            int choice;
            while (true)
            {
                Console.WriteLine("\r\r\r");
                Console.WriteLine("****************************************************");
                Console.WriteLine("Uit welk hoofdstuk wil je oefeningen laten uitvoeren?");
                Console.WriteLine("****************************************************");
                Console.WriteLine("1. Smaakmakers OOP");
                Console.WriteLine("2. Leren werken met DateTime objecten(H8-Klassen en Objecten");
                Console.WriteLine("3. Methoden, Acces Modifiers en Properties (H8-Klassen en Objecten");
                Console.WriteLine("4. GeheugenManagement bij klasses (H9)");
                Console.WriteLine("5. Geavanceerde klassen en objecten (H10)");
                Console.Write("\rKeuze: ");

                choice = int.Parse(Console.ReadLine());
                switch (choice)
                {
                    case 1:
                        SmaakmakerOOP.StartSubmenu();
                        break;

                    case 2:
                        LerenWerkenMetDateTime.StartSubmenu();
                        break;

                    case 3:
                        MethodenAccesModifiersProperties.StartSubmenu();
                        break;

                    case 4:
                        GeheugenmanagementBijClasses.StartSubmenu();
                        break;

                    case 5:
                        GeavanceerdeKlassenEnObjecten.StartSubmenu();
                        break;

                    default:
                        Console.WriteLine("Ongeldige keuze!");
                        break;
                }
            }
        }
    }
}