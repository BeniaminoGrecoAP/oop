﻿using System;

namespace OOP
{
    internal class SmaakmakerOOP
    {
        public static void StartSubmenu()
        {
            Console.Clear();
            Console.WriteLine("********************************");
            Console.WriteLine("Welke oefening wil je uitvoeren?");
            Console.WriteLine("********************************");
            Console.WriteLine("1. ShapeBuilder (H8)");
            Console.WriteLine("2. Wagen (H8)");
            int choice = int.Parse(Console.ReadLine());

            switch (choice)
            {
                case 1:
                    Console.Clear();
                    ShapesBuilder.Main();
                    break;

                case 2:
                    Console.Clear();
                    Car.Main();
                    break;

                case 3:
                    Console.Clear();
                    Pokemon.DemoRestoreHP();
                    break;

                case 4:
                    Console.Clear();
                    Pokemon.DemoFightOutcome();
                    break;

                default:
                    Console.WriteLine("Ongeldige keuze.");
                    break;
            }
        }
    }
}