﻿using System;

namespace OOP
{
    internal class Car
    {
        private double speed;

        public double Speed
        {
            get { return speed; }
            set
            {
                if (value < 0)
                {
                    speed = 0;
                }
                else if (value > 120)
                {
                    speed = 120;
                }
                else
                {
                    speed = value;
                }
            }
        }

        public double Odometer { get; set; }

        public void Gas()
        {
            double hulp = Speed;
            Speed += 10;
            Odometer += ((hulp + Speed) / 2) / 60;
        }

        public void Brake()
        {
            double hulp = Speed;
            Speed -= 10;
            Odometer += ((hulp + Speed) / 2) / 60;
        }

        public static void Main()
        {
            Car car1 = new Car();
            car1.Gas();
            car1.Gas();
            car1.Gas();
            car1.Gas();
            car1.Gas();
            car1.Brake();
            car1.Brake();
            car1.Brake();
            Console.WriteLine($"Auto 1 --> snelheid nu: {car1.Speed} , afgelegde weg: {car1.Odometer}");

            Car car2 = new Car();
            car2.Gas();
            car2.Gas();
            car2.Gas();
            car2.Gas();
            car2.Gas();
            car2.Gas();
            car2.Gas();
            car2.Gas();
            car2.Gas();
            car2.Gas();
            car2.Brake();
            car2.Brake();
            Console.WriteLine($"Auto 1 --> snelheid nu: {car2.Speed} , afgelegde weg: {car2.Odometer}");
        }
    }
}