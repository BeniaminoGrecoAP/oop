﻿using System;

namespace OOP
{
    internal class ShapesBuilder
    {
        private ConsoleColor kleur;

        public ConsoleColor Kleur
        {
            get { return kleur; }
            set
            {
                kleur = value;
                Console.ForegroundColor = kleur;
            }
        }

        public static string Lijn(int lengte)
        {
            string lijn = "";
            for (int i = 0; i < lengte; i++)
            {
                lijn += "-";
            }

            return lijn;
        }

        public static void Rechthoek(int hoogte, int breedte)
        {
            for (int rij = 0; rij < hoogte; rij++)
            {
                for (int symbool = 0; symbool < breedte; symbool++)
                {
                    Console.Write("*");
                }
                Console.WriteLine();
            }
        }

        public static void Rechthoek(int hoogte, int breedte, string teken)
        {
            for (int rij = 0; rij < hoogte; rij++)
            {
                for (int symbool = 0; symbool < breedte; symbool++)
                {
                    Console.Write(teken);
                }
                Console.WriteLine();
            }
        }

        public static void Rechthoek(int hoogte, int breedte, ConsoleColor kleur)
        {
            Console.ForegroundColor = kleur;

            for (int rij = 0; rij < hoogte; rij++)
            {
                for (int symbool = 0; symbool < breedte; symbool++)
                {
                    Console.Write("*");
                }
                Console.WriteLine();
            }
            Console.ResetColor();
        }

        public static void Rechthoek(int hoogte, int breedte, ConsoleColor kleur, string teken)
        {
            Console.ForegroundColor = kleur;

            for (int rij = 0; rij < hoogte; rij++)
            {
                for (int symbool = 0; symbool < breedte; symbool++)
                {
                    Console.Write(teken);
                }
                Console.WriteLine();
            }
            Console.ResetColor();
        }

        public static void Main()
        {
            Rechthoek(5, 5);
            Rechthoek(4, 6, "+");
            Rechthoek(10, 5, ConsoleColor.Red);
            Rechthoek(7, 7, ConsoleColor.DarkGreen, "$");
        }
    }
}